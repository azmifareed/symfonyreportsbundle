<?php
/**
 * @author Azmi Fareed
 * @copyright 2012-2014 Procept pty ltd
 * @link http://www.procept.com.au
 *
 * 
 */

namespace Drillsight\ReportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
use Drillsight\SystemBundle\Entity\Link;
use Doctrine\Common\Collections\ArrayCollection;

class StripeTransactionReportController extends Controller
{
    /**
     * Action method for Activity report
     * @param Request $request
     * @param $portal
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function indexAction(Request $request, $portal)
	{
		
		// Add Links to content header
		$links = new ArrayCollection();
        $links->add(new Link("Back", "_".$portal."_report", NULL,NULL,NULL,NULL,"ic_action_back_small.png"));	
		

		if (!$this->getUser()->moduleAccess('MOD_REPORT')) {
			throw new AccessDeniedException();
		}

		// Check if logged user has privilege to act
		//  Report Privileges are: REPORT_VIEW, REPORT_ADD, REPORT_EDIT, REPORT_DELETE
		if (!$this->getUser()->checkAccess('REPORT_VIEW', $this->getUser()->getCompany())) {
			throw new AccessDeniedException();
		}

		$em = $this->getDoctrine()->getManager();
		$stripe_helper = $this->get('stripe_helper');
		

		//Create Company Array with Company Name and ID to populate ChoiceList
		$allCompany = $em->getRepository('DrillsightCompanyBundle:Company')->findAll();
		
		$companyNameArray = array();
		$companyIdArray = array();
		array_push($companyNameArray, 'All Company');
		array_push($companyIdArray, 0);
		
		foreach($allCompany as $company)
		{
			array_push($companyNameArray, $company->getName());
			array_push($companyIdArray, $company->getId());
		}


		$filter = array();		
		//Create filter form with Company and Date Range fields
		$form = $this->createFormBuilder($filter)	
			->add('company', 'choice', array('choice_list' => new ChoiceList($companyIdArray, $companyNameArray), 'attr' => array('class' =>'form-control')))
            ->add('fromDate', 'date', array(
                'data' => new \DateTime()
            ))
            ->add('toDate', 'date', array(
                'data' => new \DateTime()
            ))
            ->add('reportPDF', 'submit', array('label' => 'View as PDF', 'attr' => array('class'=> "btn btn-primary")))
			->add('reportHTML', 'submit', array('label' => 'Generate Report', 'attr' => array('class'=> "btn btn-primary")))
			->add('reportCSV', 'submit', array('label' => 'Download CSV', 'attr' => array('class'=> "btn btn-primary")))
			->add('cancel', 'submit', array('label' => 'Cancel', 'attr' => array('class'=> "btn btn-warning")))
            ->getForm();
		
		$transactionList = array();
		$company = NULL;
		$companyName = "All Companies";
		
		
		//Handle form submission
		$form -> handleRequest($request);	
		if ($form->isValid())
		{
			if($form->get('cancel')->isClicked())   // If Cancel button is clicked return to report list page
			{
				return $this -> redirect($this -> generateUrl('_'.$portal.'_report'));
			}				
			
			$filter = $form->getData();
			
			if ($filter['company'] != 0)
			{
			    $company = $em->getRepository('DrillsightCompanyBundle:Company')->findCompany($filter['company']); 
				$companyName = $company->getName();
			}
			
			/*
			 * Call function in Stripe Bundle Helper to get the list of transactions for requested filter criteria
			 */
			/*
			$transactionList = $stripe_helper->getTransactionList($company, $filter['fromDate'], $filter['toDate'], $limit=5);
			if (array_key_exists('error', $transactionList)) {		//If Stripe returns an error display error
				$this->get('session')->getFlashBag()->add('error', $transactionList['error']['message']);
				return $this->render('DrillsightReportBundle:StripeTransactionReport:index.html.twig', array(
		            'transactionList' => $transactionList,
		        ));
			}*/
			
			$transactionList = $em->getRepository('DrillsightStripeBundle:Transaction')->findTransactions($filter);
			
			
			if(empty($transactionList)) // If Stripe returns an empty array display relevant message
			{
                $this->get('session')->getFlashBag()->add('error', 'There were no results returned for the selected period');
            }
			else 
			{
				if($form->get('reportPDF')->isClicked())	// If PDF button is clicked generate a PDF file of the Transaction list 
				{
					$templatePDF = 'DrillsightReportBundle:StripeTransactionReport:pdf_template.html.twig';
					$pdf = $this -> renderView($templatePDF, array(
	                    'toDate'=>$filter['toDate'],
	                    'fromDate'=>$filter['fromDate'],
					    'companyName'=>$companyName,
	                    'userName'=>$this -> getUser()->getName(),
					    'portal' => $portal,
	                    'transactionList' => $transactionList, 
            			'company'=>$company,
	                ));
					return new Response($this -> get('knp_snappy.pdf') -> getOutputFromHtml($pdf), 200, array(
	                    'Content-Type' => 'application/pdf',
						'Content-Disposition' => 'attachment;filename="StripeTransactionReport.pdf"'
	                ));
				} 
				if($form->get('reportCSV')->isClicked())   // If Cancel button is clicked return to report list page
				{
					// Download the Master File - it will need to be constructed into a CSV based on its template
	
		            $csv = $stripe_helper->buildDownloadFile($transactionList);
		
		            $response = new Response();
		            $response->headers->set('Content-Type', 'text/csv');
		            $response->headers->set('Content-Disposition', 'attachment;filename=StripeTransactionReport.csv');
		
		            $response->setContent($csv);
		            return $response;
				}				
							
			}
		}		        

		return $this->render('DrillsightReportBundle:StripeTransactionReport:index.html.twig', array(
            'form' => $form->createView(), 
            'portal' => $portal, 
            'transactionList' => $transactionList,
            'links' => $links, 
            'company'=>$company,));
		
	}
	
}